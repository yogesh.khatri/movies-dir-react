import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'

class EditPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            movie: {},
        }
    }
    // getting Movie details
    componentDidMount() {
        let url = "http://localhost:4000/movies/" + this.props.params.id
        fetch(url)
            .then((res) => res.json())
            .then((data) => {
                this.setState({
                    movie: data,
                });

            })
    }
    // Handling funciton to edit value
    handelChange = (e) => {
        console.log('In handle change')
        let newvalue = {}
        Object.assign(newvalue, this.state.movie)
        newvalue[e.target.name] = e.target.value
        this.setState({
            movie: newvalue
        },
        )
    }

    handelSubmit = (e) => {
        e.preventDefault()
        console.log(this.props)
        console.log(JSON.stringify(this.state.movie))
        let url = "http://localhost:4000/movies/" + this.props.params.id
        fetch(url, {
            method: 'PUT',
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state.movie)
        })
            .then((data) => {
                // console.log(data);
                console.log(this.props.history);
                this.props.history.push('/movies')
            })
    }

    render() {
        return (
            <div>
                <div>
                    <form onSubmit={this.handelSubmit} onChange={this.handelChange}>
                        <div style={{ display: "flex" }}>
                            <div style={{ width: "40%" }}> Rank</div>
                            <input value={this.state.movie.rank} name="rank"
                                style={{ width: "40%" }}></input>
                        </div>
                        <div style={{ display: "flex" }}>
                            <div style={{ width: "40%" }}> ID</div>
                            <input value={this.state.movie.id} name="id"
                                style={{ width: "40%" }}></input>
                        </div>
                        <div style={{ display: "flex" }}>
                            <div style={{ width: "40%" }}> Tilte</div>
                            <input value={this.state.movie.title} name="title"
                                style={{ width: "40%" }}></input>
                        </div>
                        <div style={{ display: "flex" }}>
                            <div style={{ width: "40%" }}> description</div>
                            <input value={this.state.movie.description} name="description"
                                style={{ width: "40%" }}></input>
                        </div>
                        <div style={{ display: "flex" }}>
                            <div style={{ width: "40%" }}> runtime</div>
                            <input value={this.state.movie.runtime} name="runtime"
                                style={{ width: "40%" }}></input>
                        </div>
                        <div style={{ display: "flex" }}>
                            <div style={{ width: "40%" }}> genre</div>
                            <input value={this.state.movie.genre} name="genre"
                                style={{ width: "40%" }}></input>
                        </div>
                        <div style={{ display: "flex" }}>
                            <div style={{ width: "40%" }}> rating</div>
                            <input value={this.state.movie.rating} name="rating"
                                style={{ width: "40%" }}></input>
                        </div>
                        <div style={{ display: "flex" }}>
                            <div style={{ width: "40%" }}> metascore</div>
                            <input value={this.state.movie.metascore} name="metascore"
                                style={{ width: "40%" }}></input>
                        </div>
                        <div style={{ display: "flex" }}>
                            <div style={{ width: "40%" }}> votes</div>
                            <input value={this.state.movie.votes} name="votes"
                                style={{ width: "40%" }}></input>
                        </div>
                        <div style={{ display: "flex" }}>
                            <div style={{ width: "40%" }}> gross earning</div>
                            <input value={this.state.movie.gross_earning_in_mil} name="gross_earning_in_mil"
                                style={{ width: "40%" }}></input>
                        </div>
                        <div style={{ display: "flex" }}>
                            <div style={{ width: "40%" }}> actor</div>
                            <input value={this.state.movie.actor} name="actor"
                                style={{ width: "40%" }}></input>
                        </div>
                        <div style={{ display: "flex" }}>
                            <div style={{ width: "40%" }}> year</div>
                            <input value={this.state.movie.year} name="year"
                                style={{ width: "40%" }}></input>
                        </div>
                        <div style={{ display: "flex" }}>
                            <div style={{ width: "40%" }}> director_id</div>
                            <input value={this.state.movie.director_id} name="director_id"
                                style={{ width: "40%" }}></input>
                        </div>
                        <input type="submit" value="submit" />

                    </form>
                </div>
            </div>
        )
    }
}

export default withRouter(EditPage)
