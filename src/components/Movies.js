import React, { Component } from 'react';
import MoviesDiv from './MoviesDiv';
// import { Redirect } from 'react-router-dom'
import { Link } from 'react-router-dom';

class Movies extends Component {
    state = {
        movies: [],
    }
    // getting all Movies details
    componentDidMount() {
        let url = "http://localhost:4000/movies"
        fetch(url)
            .then((res) => res.json())
            .then((data) => {
                this.setState({
                    movies: data,
                })
            })
    }
    // delete function
    deleteButton = (id) => {
        let url = "http://localhost:4000/movies/" + id;
        fetch(url, {
            method: 'DELETE'

        })
            .then((res) => {
                this.componentDidMount();
                // <Redirect to='/movies' />
            })
    }
    // rander movies function
    renderMoviesDiv(movie) {
        return (
            < MoviesDiv
                movie={movie}
                key={movie.id}
                deleteButton={this.deleteButton}
            />)
    }

    render() {
        return (
            <div>
                <div>All Movies</div>
                <Link to='/addMovie'>
                    <button>Add Movies</button>
                </Link>
                <div style={{
                    display: "flex",
                    flexWrap: "wrap"
                }}>
                    {
                        this.state.movies.map((movie) => {
                            return this.renderMoviesDiv(movie)
                        })
                    }
                </div>
            </div>
        )
    }
}

export default Movies


