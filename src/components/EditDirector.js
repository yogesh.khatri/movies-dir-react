import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'

class EditDirector extends Component {
    state = {
        director: {},
    }

    // getting Directors details
    componentDidMount() {
        let url = "http://localhost:4000/directors/" + this.props.params.id
        fetch(url)
            .then((res) => res.json())
            .then((data) => {
                this.setState({
                    director: data,
                });

            })
    }

    // Handling funciton to edit value
    handelChange = (e) => {
        console.log('In handle change')
        let newvalue = {}
        Object.assign(newvalue, this.state.director)
        newvalue[e.target.name] = e.target.value
        this.setState({
            director: newvalue
        })
    }
    // Handle Submit 
    handelSubmit = (e) => {
        e.preventDefault()
        console.log(this.props)
        console.log(JSON.stringify(this.state.director))
        let url = "http://localhost:4000/directors/" + this.props.params.id
        fetch(url, {
            method: 'PUT',
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state.director)
        })
            .then((data) => {
                // console.log(data);
                console.log(this.props.history);
                this.props.history.push('/directors')
            })
    }

    render() {
        console.log("In edit")
        return (
            <div>
                <div>
                    <form onSubmit={this.handelSubmit} onChange={this.handelChange}>
                        <div style={{ display: "flex" }}>
                            <div style={{ width: "40%" }}> Director Name</div>
                            <input value={this.state.director.director} name="director"
                                style={{ width: "40%" }}></input>
                        </div>
                        <input type="submit" value="submit" />
                    </form>
                </div>
            </div>
        )
    }
}

export default withRouter(EditDirector)
