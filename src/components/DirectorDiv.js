import React, { Component } from 'react'
import { Link } from "react-router-dom"

class DirectorDiv extends Component {

    // object to key-value pair list
    ObjectToList(director) {
        let ListOfDirectorsDetails = []
        for (let property in director) {
            let temp = [];
            temp.push(property);
            temp.push(director[property]);
            ListOfDirectorsDetails.push(temp)
        }
        return ListOfDirectorsDetails;
    }
    render() {
        return (
            <div style={{
                borderStyle: "solid",
                backgroundColor: "beige",
                width: "40%",
                margin: "3%",
                padding: "1%"
            }}>
                {
                    this.ObjectToList(this.props.director).map((property) => {
                        return (
                            <DirectorDivItem
                                property={property[0]}
                                value={property[1]}
                                key={property[0]}
                            />)
                    })
                }
                <Link to={{
                    pathname: `directors/${this.props.director.id}`,
                }}>
                    <button>Edit</button>
                </Link>
                <button onClick={this.props.deleteButton.bind(this, this.props.director.id)}>Delete</button>
            </div>
        )
    }
}

// Creating directors items
function DirectorDivItem(props) {
    return (
        <div style={{
            display: "flex",
        }}>
            <div style={{
                width: "40%"
            }}
            > {props.property}</div> :
                <div
                style={{
                    width: "40%"
                }}
            >{props.value}</div>
        </div>
    )
}

export default DirectorDiv
