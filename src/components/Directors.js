import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import DirectorDiv from './DirectorDiv'

class Directors extends Component {
    state = {
        directors: []
    }
    // Fetching directors list 
    componentDidMount() {
        let url = "http://localhost:4000/directors";
        fetch(url)
            .then((res) => res.json())
            .then((data) => {
                this.setState({
                    directors: data
                })
            })
    }

    // delete function
    deleteButton = (id) => {
        let url = "http://localhost:4000/directors/" + id;
        fetch(url, {
            method: 'DELETE'
        })
            .then((res) => {
                this.componentDidMount();
            })
    }

    render() {
        return (
            <div>
                <div>All Directors</div>
                <Link to='/addDirectors'>
                    <button>Add Directors</button>
                </Link>
                <div style={{
                    display: "flex",
                    flexWrap: "wrap"
                }}>
                    {
                        this.state.directors.map((director) => {
                            return <DirectorDiv
                                director={director}
                                key={director.id}
                                deleteButton={this.deleteButton}
                            />
                        })
                    }
                </div>
            </div>
        )
    }
}

export default Directors
