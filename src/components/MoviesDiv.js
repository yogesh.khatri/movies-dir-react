import React, { Component } from 'react'
import { Link } from "react-router-dom"

class MoviesDiv extends Component {
    //rendering properties to MoviesDivItem 
    renderProperties(property, value) {
        return (
            <MoviesDivItem
                property={property}
                value={value}
                key={property}
            />)
    }

    // object to key-value pair list
    ObjectToList(movie) {
        let ListOfMovieDetails = []
        for (let property in movie) {
            let temp = [];
            temp.push(property);
            temp.push(movie[property]);

            ListOfMovieDetails.push(temp)
        }
        return ListOfMovieDetails;
    }

    render() {
        return (
            <div className="movieBox"
                style={{
                    borderStyle: "solid",
                    backgroundColor: "beige",
                    width: "40%",
                    margin: "3%",
                    padding: "1%"
                }}>

                {
                    this.ObjectToList(this.props.movie).map((property) => {
                        return this.renderProperties(property[0], property[1])
                    })
                }
                <Link to={{
                    pathname: `movies/${this.props.movie.id}`,
                }}>
                    <button>Edit</button>
                </Link>
                <button onClick={this.props.deleteButton.bind(this, this.props.movie.id)}>Delete</button>

            </div>
        );
    }
}

// creating items one by one
function MoviesDivItem(props) {
    return (
        <div style={{
            display: "flex",
        }}>
            <div
                style={{
                    width: "40%"
                }}
            > {props.property}</div> :
                <div
                style={{
                    width: "40%"
                }}
            >{props.value}</div>
        </div>
    );
}

export default MoviesDiv
