import React, { Component } from 'react'
// import withRouter from 'react-router-dom'

class AddDirector extends Component {
    state = {
        director: null,
    }

    // Handling funciton to edit value
    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    // Posting value on Submit
    handelSubmit = (e) => {
        e.preventDefault()
        // console.log(this.state)
        let url = "http://localhost:4000/directors"
        fetch(url, {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state)
        })
            .then((data) => {
                // console.log(this.props)
                this.props.history.push('/directors');
            })
    }
    render() {
        return (
            <div>
                <form onSubmit={this.handelSubmit} onChange={this.handleChange} >
                    <div style={{ display: "flex" }}>
                        <div style={{ width: "40%" }}> Director</div>
                        <input name="director"
                            style={{ width: "40%" }}></input>
                    </div>
                    <input type="submit" value="submit" />

                </form>
            </div>
        )
    }
}

export default AddDirector
