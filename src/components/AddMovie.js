import React, { Component } from 'react'
// import withRouter from 'react-router-dom'

class AddMovie extends Component {
    state = {
        rank: null,
        title: null,
        description: null,
        runtime: null,
        genre: null,
        rating: null,
        metascore: null,
        votes: null,
        gross_earning_in_mil: null,
        year: null,
        director_id: null,
    }

    // Handling funciton to edit value
    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    // Posting value on Submit
    handelSubmit = (e) => {
        e.preventDefault()
        // console.log(this.state)
        let url = "http://localhost:4000/movies"
        fetch(url, {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state)
        })
            .then((data) => {
                // console.log(this.props)
                this.props.history.push('/movies');
            })
    }
    render() {
        return (
            <div>
                <form onSubmit={this.handelSubmit} onChange={this.handleChange} >
                    <div style={{ display: "flex" }}>
                        <div style={{ width: "40%" }}> Rank</div>
                        <input name="rank"
                            style={{ width: "40%" }}></input>
                    </div>
                    <div style={{ display: "flex" }}>
                        <div style={{ width: "40%" }}> Tilte</div>
                        <input name="title"
                            style={{ width: "40%" }}></input>
                    </div>
                    <div style={{ display: "flex" }}>
                        <div style={{ width: "40%" }}> description</div>
                        <input name="description"
                            style={{ width: "40%" }}></input>
                    </div>
                    <div style={{ display: "flex" }}>
                        <div style={{ width: "40%" }}> runtime</div>
                        <input name="runtime"
                            style={{ width: "40%" }}></input>
                    </div>
                    <div style={{ display: "flex" }}>
                        <div style={{ width: "40%" }}> genre</div>
                        <input name="genre"
                            style={{ width: "40%" }}></input>
                    </div>
                    <div style={{ display: "flex" }}>
                        <div style={{ width: "40%" }}> rating</div>
                        <input name="rating"
                            style={{ width: "40%" }}></input>
                    </div>
                    <div style={{ display: "flex" }}>
                        <div style={{ width: "40%" }}> metascore</div>
                        <input name="metascore"
                            style={{ width: "40%" }}></input>
                    </div>
                    <div style={{ display: "flex" }}>
                        <div style={{ width: "40%" }}> votes</div>
                        <input name="votes"
                            style={{ width: "40%" }}></input>
                    </div>
                    <div style={{ display: "flex" }}>
                        <div style={{ width: "40%" }}> gross earning</div>
                        <input name="gross_earning_in_mil"
                            style={{ width: "40%" }}></input>
                    </div>
                    <div style={{ display: "flex" }}>
                        <div style={{ width: "40%" }}> actor</div>
                        <input name="actor"
                            style={{ width: "40%" }}></input>
                    </div>
                    <div style={{ display: "flex" }}>
                        <div style={{ width: "40%" }}> year</div>
                        <input name="year"
                            style={{ width: "40%" }}></input>
                    </div>
                    <div style={{ display: "flex" }}>
                        <div style={{ width: "40%" }}> director_id</div>
                        <input name="director_id"
                            style={{ width: "40%" }}></input>
                    </div>
                    <input type="submit" value="submit" />

                </form>
            </div>
        )
    }
}

export default AddMovie
