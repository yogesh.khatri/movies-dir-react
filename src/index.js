import React from 'react';
import ReactDOM from 'react-dom';
import HomePage from './components/HomePage'
import Movies from './components/Movies';
import EditPage from './components/EditPage'
import AddMovie from './components/AddMovie'
import EditDirector from './components/EditDirector'
import AddDirector from './components/AddDirector'
import Directors from './components/Directors'
import { BrowserRouter as Router, Route } from "react-router-dom"

ReactDOM.render(
    <Router>
        <Route path='/' exact component={HomePage} />
        <Route path='/movies' exact component={Movies} />
        <Route path='/addMovie' exact component={AddMovie} />
        <Route path='/movies/:id' render={(props) => (
            <EditPage params={props.match.params} />
        )} />
        <Route path='/directors' exact component={Directors} />
        <Route path='/addDirectors' exact component={AddDirector} />
        <Route path='/directors/:id' render={(props) => (
            <EditDirector params={props.match.params} />
        )} />
    </Router>,
    document.getElementById('root')
);